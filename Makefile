.PHONY: help build release
	.DEFAULT_GOAL := build

help: ## show this message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'



build: ## build the binary
	operator-sdk generate k8s
	operator-sdk build quay.io/garyellis/pegasus-operator:devel

release: ## release the binary and docker image
	docker push quay.io/garyellis/pegasus-operator:devel

