// Package v1alpha1 contains API Schema definitions for the pegasus v1alpha1 API group
// +k8s:deepcopy-gen=package,register
// +groupName=pegasus.ews.works
package v1alpha1
