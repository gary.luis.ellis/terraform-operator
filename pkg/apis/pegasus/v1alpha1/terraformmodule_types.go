package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// TerraformModuleSpec defines the desired state of TerraformModule
// +k8s:openapi-gen=true
type TerraformModuleSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book.kubebuilder.io/beyond_basics/generating_crd.html
	Module runtime.RawExtension `json:"module,omitempty"`
	Env    map[string]string    `json:"env,omitempty"`
	Secret string               `json:"secret,omitempty"`
}

// TerraformModuleStatus defines the observed state of TerraformModule
// +k8s:openapi-gen=true
type TerraformModuleStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book.kubebuilder.io/beyond_basics/generating_crd.html
	Outputs runtime.RawExtension `json:"outputs,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TerraformModule is the Schema for the terraformmodules API
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
type TerraformModule struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   TerraformModuleSpec   `json:"spec,omitempty"`
	Status TerraformModuleStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TerraformModuleList contains a list of TerraformModule
type TerraformModuleList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []TerraformModule `json:"items"`
}

func init() {
	SchemeBuilder.Register(&TerraformModule{}, &TerraformModuleList{})
}
