package terraform

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"sync"

	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

var log = logf.Log.WithName("terraform")

const maintf = "main.tf.json"

// Executor wraps terraform initialization and commands.
type Executor struct {
	WorkDir string
	Payload []byte
	Env     []string
}

// NewTerraformExecutor returns a TerraformExecutor to run terraform commands
func NewTerraformExecutor(workdir string, env []string, paylaod []byte) *Executor {
	return &Executor{
		WorkDir: workdir,
		Payload: paylaod,
		Env:     env,
	}
}

// Init stages the terraform root module and runs the terraform init command.
func (t *Executor) Init() error {

	makeWorkDir(t.WorkDir)
	writeFile(t.Payload, fmt.Sprintf("%s/%s", t.WorkDir, maintf))

	_, err := t.terraform("init")
	if err != nil {
		log.Error(err, "terraform init failed.")
		return err
	}
	return nil
}

// Apply runs terraform apply on the terraform work directory
func (t *Executor) Apply() error {
	_, err := t.terraform("apply", "-auto-approve")
	if err != nil {
		log.Error(err, "terraform apply failed")
		return err
	}
	return nil
}

// Output runs the terraform output command.
func (t *Executor) Output() ([]byte, error) {
	b, err := t.terraform("output", "-json")
	if err != nil {
		log.Error(err, "terraform output failed")
	}
	return b, nil
}

// Destroy runs terraform destroy on the cluster infra
func (t *Executor) Destroy() error {
	_, err := t.terraform("destroy", "-auto-approve")
	if err != nil {
		log.Error(err, "terraform destroy failed")
	}
	// Need to return error to caller
	return nil
}

// terraform wraps the terraform binary
func (t *Executor) terraform(args ...string) ([]byte, error) {
	log.Info(fmt.Sprintf("running terraform %s", args))
	cmd := exec.Command("terraform", args...)
	env := os.Environ()
	for i := range t.Env {
		env = append(env, t.Env[i])
	}
	cmd.Env = env
	cmd.Dir = t.WorkDir

	var returnStdout, returnStderr []byte
	cmdStdOutReader, _ := cmd.StdoutPipe()
	cmdStdErrReader, _ := cmd.StderrPipe()

	err := cmd.Start()
	if err != nil {
		return nil, err
	}

	returnStdout, _ = ReadStdOutErrPipe(cmdStdOutReader)
	returnStderr, _ = ReadStdOutErrPipe(cmdStdErrReader)
	err = cmd.Wait()
	if err != nil {
		log.Error(err, "terraform failed.", returnStderr)
		return nil, err
	}

	return returnStdout, nil
}

// write writes the terraform configuration payload to the root module directory
func writeFile(payload []byte, path string) error {

	err := ioutil.WriteFile(path, payload, 0644)
	if err != nil {
		log.Error(err, "write terraform module contents failed")
		return err
	}
	return nil
}

// makeWorkDir creates the terraform root module directory
func makeWorkDir(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		mode := 0755
		err := os.MkdirAll(path, os.FileMode(mode))
		if err != nil {
			log.Error(err, "create module dir failed")
			return err
		}
	}
	return nil
}

// ReadStdOutErrPipe logs stdout and stderr stream and returns contents
func ReadStdOutErrPipe(r io.ReadCloser) ([]byte, error) {
	var b []byte
	s := bufio.NewScanner(r)
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for s.Scan() {
			log.Info(s.Text())
			b = append(b, s.Bytes()...)
		}
		wg.Done()
	}()
	wg.Wait()
	return b, nil
}
