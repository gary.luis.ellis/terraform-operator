package terraformmodule

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	pegasusv1alpha1 "github.com/garyellis/pegasus-operator/pkg/apis/pegasus/v1alpha1"
	"github.com/garyellis/pegasus-operator/pkg/terraform"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("controller_terraformmodule")

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new TerraformModule Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileTerraformModule{client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("terraformmodule-controller", mgr, controller.Options{MaxConcurrentReconciles: 10, Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource TerraformModule
	err = c.Watch(&source.Kind{Type: &pegasusv1alpha1.TerraformModule{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

// blank assignment to verify that ReconcileTerraformModule implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileTerraformModule{}

// ReconcileTerraformModule reconciles a TerraformModule object
type ReconcileTerraformModule struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a TerraformModule object and makes changes based on the state read
// and what is in the TerraformModule.Spec
func (r *ReconcileTerraformModule) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := log.WithValues("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Info("Reconciling TerraformModule")
	m := getMetrics()

	// Fetch the TerraformModule instance
	instance := &pegasusv1alpha1.TerraformModule{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{RequeueAfter: time.Minute * 2}, err
	}

	// Setup the terraform execution environment
	secret := &corev1.Secret{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Namespace: request.Namespace, Name: instance.Spec.Secret}, secret)
	if instance.Spec.Secret != "" && err != nil {
		log.Error(err, "Get secret failed")
		return reconcile.Result{RequeueAfter: time.Minute * 2}, err
	}

	var secretEnv map[string][]byte
	if secret.Data != nil {
		secretEnv = secret.Data
	}

	// Get the terraform executor
	tfexecutor, err := getTerraformExecutor(instance, secretEnv)
	if err != nil {
		log.Error(err, "Failed to setup the terraform execution environment")
		return reconcile.Result{RequeueAfter: time.Minute * 2}, err
	}

	// Check if the terraform module is marked for deletion
	isModuleMarkedForDeletion := instance.GetDeletionTimestamp() != nil
	if isModuleMarkedForDeletion {

		// Destroy the module and remove finalizers
		reqLogger.Info("destroyingterraform module", "module", instance.Name)
		if err := terraformDestroy(tfexecutor); err != nil {
			return reconcile.Result{RequeueAfter: time.Minute * 2}, err
		}

		// Remove the finalizer
		instance.SetFinalizers(nil)
		err := r.client.Update(context.TODO(), instance)
		if err != nil {
			log.Error(err, "remove finalizer failed")
			return reconcile.Result{}, err
		}
		return reconcile.Result{}, nil
	}

	// apply the terraform module
	reqLogger.Info("applying terraform module")
	if err := terraformApply(tfexecutor); err != nil {
		// write metrics
		log.Error(err, "terraform apply failed")
		log.Info("writing error metrics")
		m.ErrorsTotal.WithLabelValues(request.Namespace, instance.Name).Inc()

		return reconcile.Result{RequeueAfter: time.Minute * 2}, err
	}

	// Add the finalizer
	if err := r.addFinalizer(reqLogger, instance); err != nil {
		return reconcile.Result{RequeueAfter: time.Minute * 2}, err
	}

	// update the status
	reqLogger.Info("Updating the status")
	if err := r.updateStatus(reqLogger, instance, tfexecutor); err != nil {
		log.Error(err, "update status failed")
		//return reconcile.Result{RequeueAfter: time.Minute * 2}, err
	}

	m.ApplyExecutions.WithLabelValues(request.Namespace, instance.Name).Inc()
	return reconcile.Result{RequeueAfter: time.Minute * 2}, nil
}

func (r *ReconcileTerraformModule) addFinalizer(reqLogger logr.Logger, cr *pegasusv1alpha1.TerraformModule) error {
	if len(cr.GetFinalizers()) < 1 && cr.GetDeletionTimestamp() == nil {
		reqLogger.Info("Adding the terraform module finalizer")
		cr.SetFinalizers([]string{"finalizer.terraformmodules.pegasus.ews.works"})
	}

	err := r.client.Update(context.TODO(), cr)
	if err != nil {
		reqLogger.Error(err, "Failed to update terraformmodule with its finalizer")
		return err
	}
	return nil
}

func terraformApply(tfexecutor *terraform.Executor) error {
	err := tfexecutor.Apply()
	if err != nil {
		log.Error(err, "terraform apply failed")
		return err
	}
	return nil
}

func terraformDestroy(tfexecutor *terraform.Executor) error {
	if err := tfexecutor.Destroy(); err != nil {
		log.Error(err, "terraform destroy failed")
		return err
	}
	return nil
}

func getTerraformExecutor(cr *pegasusv1alpha1.TerraformModule, secretEnv map[string][]byte) (*terraform.Executor, error) {
	workDir := fmt.Sprintf("/tmp/terraform-module/%s", cr.Name)
	tfjson, err := json.Marshal(cr.Spec.Module)
	if err != nil {
		log.Error(err, "Unmarshal terraform module failed")
		return nil, err
	}

	var env []string
	for k, v := range cr.Spec.Env {
		env = append(env, fmt.Sprintf("%s=%s", k, v))
	}

	if secretEnv != nil {
		for k, v := range secretEnv {
			env = append(env, fmt.Sprintf("%s=%s", k, string(v)))
		}
	}

	tfexecutor := terraform.NewTerraformExecutor(workDir, env, tfjson)
	err = tfexecutor.Init()
	if err != nil {
		log.Error(err, "terraform init failed")
		return nil, err
	}
	return tfexecutor, nil
}

func (r *ReconcileTerraformModule) updateStatus(reqLogger logr.Logger, cr *pegasusv1alpha1.TerraformModule, tfexecutor *terraform.Executor) error {
	outputs := getTerraformOutputs(reqLogger, tfexecutor)
	cr.Status.Outputs = outputs

	reqLogger.Info(fmt.Sprintf("status outputs: %s", cr.Status.Outputs))
	err := r.client.Status().Update(context.TODO(), cr)
	if err != nil {
		reqLogger.Error(err, "Update status failed")
		return err
	}
	return nil
}

func getTerraformOutputs(reqLogger logr.Logger, tfexecutor *terraform.Executor) runtime.RawExtension {
	var outputs runtime.RawExtension
	outputsIn, err := tfexecutor.Output()
	if err != nil {
		reqLogger.Error(err, "getTerraformOutputs Failed get terraform outputs")
	}
	outputs.Raw = outputsIn
	return outputs
}
