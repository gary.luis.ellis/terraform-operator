package terraformmodule

import (
	"context"
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/operator-framework/operator-sdk/pkg/k8sutil"
	v1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	crclient "sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

var Registry = prometheus.NewRegistry()
var Metrics = InitTerraformMetrics("terraform")

type TerraformMetrics struct {
	Prefix                string
	ErrorsTotal           *prometheus.CounterVec
	ResourcesCreated      *prometheus.GaugeVec
	ResourcesUpdated      *prometheus.GaugeVec
	ResourcesDeleted      *prometheus.GaugeVec
	ResourcesCreatedTotal *prometheus.CounterVec
	ApplyExecutions       *prometheus.CounterVec
	// moduleexecutionduration
}

func InitTerraformMetrics(prefix string) *TerraformMetrics {
	ptm := TerraformMetrics{
		Prefix: prefix,
		ErrorsTotal: promauto.NewCounterVec(prometheus.CounterOpts{
			Name: prefix + "_module_errors",
			Help: "Terraform module execution errors",
		}, []string{"namespace", "module"}),
		ResourcesCreated: promauto.NewGaugeVec(prometheus.GaugeOpts{
			Name: prefix + "_module_resources_created",
			Help: "Terraform module resources created",
		}, []string{"namespace", "module"}),
		ResourcesUpdated: promauto.NewGaugeVec(prometheus.GaugeOpts{
			Name: prefix + "_module_resources_updated",
			Help: "Terraform module resources updated",
		}, []string{"namespace", "module"}),
		ResourcesDeleted: promauto.NewGaugeVec(prometheus.GaugeOpts{
			Name: prefix + "_module_resources_deleted",
			Help: "Terraform module resources deleted",
		}, []string{"namespace", "module"}),
		ResourcesCreatedTotal: promauto.NewCounterVec(prometheus.CounterOpts{
			Name: prefix + "_module_resources_total",
			Help: "Terraform module total resources created counter",
		}, []string{"namespace", "module"}),
		ApplyExecutions: promauto.NewCounterVec(prometheus.CounterOpts{
			Name: prefix + "_module_apply_executions",
			Help: "Terraform module apply successful executions",
		}, []string{"namespace", "module"}),
	}

	return &ptm
}

func getMetrics() *TerraformMetrics {
	return Metrics
}

// Registers terraform metric collectors
func registerClientMetrics() {
	Registry.MustRegister(Metrics.ErrorsTotal)
	Registry.MustRegister(Metrics.ResourcesCreated)
	Registry.MustRegister(Metrics.ResourcesCreatedTotal)
	Registry.MustRegister(Metrics.ResourcesDeleted)
	Registry.MustRegister(Metrics.ResourcesUpdated)
	Registry.MustRegister(Metrics.ApplyExecutions)
}

func init() {
	registerClientMetrics()
}

// ServeMetrics serves prometheus metrics for the terraformmodule controller
func ServeMetrics(port int32) {
	handler := promhttp.HandlerFor(Registry, promhttp.HandlerOpts{
		ErrorHandling: promhttp.HTTPErrorOnError,
	})
	mux := http.NewServeMux()
	mux.Handle("/metrics", handler)
	server := http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: mux,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			// log the error
		}
	}()
}

/*
Review of controller-runtime manager metrics setup.
https://github.com/kubernetes-sigs/controller-runtime/blob/master/pkg/manager/manager.go#L162
https://github.com/kubernetes-sigs/controller-runtime/blob/master/pkg/manager/manager.go#L247
https://github.com/kubernetes-sigs/controller-runtime/blob/master/pkg/manager/internal.go#L229


## instrumenting code blogs
https://blog.pvincent.io/2017/12/prometheus-blog-series-part-4-instrumenting-code-in-go-and-java/
https://scot.coffee/2018/12/monitoring-go-applications-with-prometheus/
*/

func ExposeMetricsPort(ctx context.Context, port int32) error {
	client, err := createClient()
	if err != nil {
		return fmt.Errorf("failed to create new client: %v", err)
	}

	s, err := initOperatorService(ctx, client, port, "terraform-metrics")
	if err != nil {
		if err == k8sutil.ErrNoNamespace {
			log.Info("Skipping metrics Service creation; not running in a cluster.")
			return nil
		}
		return fmt.Errorf("failed to initialize service object for metrics: %v", err)
	}
	_, err = createOrUpdateService(ctx, client, s)
	if err != nil {
		return fmt.Errorf("failed to create or get service for metrics: %v", err)
	}

	return nil
}
func createOrUpdateService(ctx context.Context, client crclient.Client, s *v1.Service) (*v1.Service, error) {
	if err := client.Create(ctx, s); err != nil {
		if !apierrors.IsAlreadyExists(err) {
			return nil, err
		}
		// Service already exists, we want to update it
		// as we do not know if any fields might have changed.
		existingService := &v1.Service{}
		err := client.Get(ctx, types.NamespacedName{
			Name:      s.Name,
			Namespace: s.Namespace,
		}, existingService)

		s.ResourceVersion = existingService.ResourceVersion
		if existingService.Spec.Type == v1.ServiceTypeClusterIP {
			s.Spec.ClusterIP = existingService.Spec.ClusterIP
		}
		err = client.Update(ctx, s)
		if err != nil {
			return nil, err
		}
		log.V(1).Info("Metrics Service object updated", "Service.Name", s.Name, "Service.Namespace", s.Namespace)
		return existingService, nil
	}

	log.Info("Metrics Service object created", "Service.Name", s.Name, "Service.Namespace", s.Namespace)
	return s, nil
}

func initOperatorService(ctx context.Context, client crclient.Client, port int32, portName string) (*v1.Service, error) {
	operatorName, err := k8sutil.GetOperatorName()
	if err != nil {
		return nil, err
	}
	namespace, err := k8sutil.GetOperatorNamespace()
	if err != nil {
		return nil, err
	}

	serviceName := fmt.Sprintf("%s-terraform", operatorName)
	label := map[string]string{"name": operatorName}

	service := &v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      serviceName,
			Namespace: namespace,
			Labels:    label,
		},
		Spec: v1.ServiceSpec{
			Ports: []v1.ServicePort{
				{
					Port:     port,
					Protocol: v1.ProtocolTCP,
					TargetPort: intstr.IntOrString{
						Type:   intstr.Int,
						IntVal: port,
					},
					Name: portName,
				},
			},
			Selector: label,
		},
	}

	ownRef, err := getPodOwnerRef(ctx, client, namespace)
	if err != nil {
		return nil, err
	}
	service.SetOwnerReferences([]metav1.OwnerReference{*ownRef})

	return service, nil
}

func getPodOwnerRef(ctx context.Context, client crclient.Client, ns string) (*metav1.OwnerReference, error) {
	// Get current Pod the operator is running in
	pod, err := k8sutil.GetPod(ctx, client, ns)
	if err != nil {
		return nil, err
	}
	podOwnerRefs := metav1.NewControllerRef(pod, pod.GroupVersionKind())
	// Get Owner that the Pod belongs to
	ownerRef := metav1.GetControllerOf(pod)
	finalOwnerRef, err := findFinalOwnerRef(ctx, client, ns, ownerRef)
	if err != nil {
		return nil, err
	}
	if finalOwnerRef != nil {
		return finalOwnerRef, nil
	}

	// Default to returning Pod as the Owner
	return podOwnerRefs, nil
}

func findFinalOwnerRef(ctx context.Context, client crclient.Client, ns string, ownerRef *metav1.OwnerReference) (*metav1.OwnerReference, error) {
	if ownerRef == nil {
		return nil, nil
	}

	obj := &unstructured.Unstructured{}
	obj.SetAPIVersion(ownerRef.APIVersion)
	obj.SetKind(ownerRef.Kind)
	err := client.Get(ctx, types.NamespacedName{Namespace: ns, Name: ownerRef.Name}, obj)
	if err != nil {
		return nil, err
	}
	newOwnerRef := metav1.GetControllerOf(obj)
	if newOwnerRef != nil {
		return findFinalOwnerRef(ctx, client, ns, newOwnerRef)
	}

	log.V(1).Info("Pods owner found", "Kind", ownerRef.Kind, "Name", ownerRef.Name, "Namespace", ns)
	return ownerRef, nil
}

func createClient() (crclient.Client, error) {
	config, err := config.GetConfig()
	if err != nil {
		return nil, err
	}

	client, err := crclient.New(config, crclient.Options{})
	if err != nil {
		return nil, err
	}

	return client, nil
}
