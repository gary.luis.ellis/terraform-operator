package controller

import (
	"github.com/garyellis/pegasus-operator/pkg/controller/terraformmodule"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, terraformmodule.Add)
}
